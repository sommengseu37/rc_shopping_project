<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrderDetailsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('order_details', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger("customer_id")->nullable();
            $table->unsignedBigInteger("payment_id")->nullable();
            $table->string("qty");
            $table->string("total");
            $table->unsignedBigInteger("pay_by_id")->nullable();
            $table->timestamps();

            $table->foreign("customer_id")->references("id")->on("customers");
            $table->foreign("payment_id")->references("id")->on("payments");
            $table->foreign("pay_by_id")->references("id")->on("pay_by");
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('order_details');
    }
}
